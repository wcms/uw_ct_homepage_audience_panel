<?php

/**
 * @file
 * uw_ct_homepage_audience_panel.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_homepage_audience_panel_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create homepage_audience_panel content'.
  $permissions['create homepage_audience_panel content'] = array(
    'name' => 'create homepage_audience_panel content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any homepage_audience_panel content'.
  $permissions['delete any homepage_audience_panel content'] = array(
    'name' => 'delete any homepage_audience_panel content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own homepage_audience_panel content'.
  $permissions['delete own homepage_audience_panel content'] = array(
    'name' => 'delete own homepage_audience_panel content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in homepage_audience_panel_audiences'.
  $permissions['delete terms in homepage_audience_panel_audiences'] = array(
    'name' => 'delete terms in homepage_audience_panel_audiences',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any homepage_audience_panel content'.
  $permissions['edit any homepage_audience_panel content'] = array(
    'name' => 'edit any homepage_audience_panel content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own homepage_audience_panel content'.
  $permissions['edit own homepage_audience_panel content'] = array(
    'name' => 'edit own homepage_audience_panel content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in homepage_audience_panel_audiences'.
  $permissions['edit terms in homepage_audience_panel_audiences'] = array(
    'name' => 'edit terms in homepage_audience_panel_audiences',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter homepage_audience_panel revision log entry'.
  $permissions['enter homepage_audience_panel revision log entry'] = array(
    'name' => 'enter homepage_audience_panel revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_audience_panel authored by option'.
  $permissions['override homepage_audience_panel authored by option'] = array(
    'name' => 'override homepage_audience_panel authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_audience_panel authored on option'.
  $permissions['override homepage_audience_panel authored on option'] = array(
    'name' => 'override homepage_audience_panel authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_audience_panel promote to front page option'.
  $permissions['override homepage_audience_panel promote to front page option'] = array(
    'name' => 'override homepage_audience_panel promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_audience_panel published option'.
  $permissions['override homepage_audience_panel published option'] = array(
    'name' => 'override homepage_audience_panel published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_audience_panel revision option'.
  $permissions['override homepage_audience_panel revision option'] = array(
    'name' => 'override homepage_audience_panel revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_audience_panel sticky option'.
  $permissions['override homepage_audience_panel sticky option'] = array(
    'name' => 'override homepage_audience_panel sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  return $permissions;
}
