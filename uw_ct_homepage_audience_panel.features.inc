<?php

/**
 * @file
 * uw_ct_homepage_audience_panel.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_homepage_audience_panel_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_homepage_audience_panel_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_homepage_audience_panel_node_info() {
  $items = array(
    'homepage_audience_panel' => array(
      'name' => t('Audience Panel Items'),
      'base' => 'node_content',
      'description' => t('Shares timely and relevant information pertaining to the selected audience (future students, current students, faculty, staff, alumni, employers). Content directs users to news, stories, opportunities, events, important dates, and useful information.'),
      'has_title' => '1',
      'title_label' => t('Headline'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
