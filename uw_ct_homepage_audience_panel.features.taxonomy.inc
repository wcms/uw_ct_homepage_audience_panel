<?php
/**
 * @file
 * uw_ct_homepage_audience_panel.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_ct_homepage_audience_panel_taxonomy_default_vocabularies() {
  return array(
    'homepage_audience_panel_audiences' => array(
      'name' => 'Audience Panel Audiences',
      'machine_name' => 'homepage_audience_panel_audiences',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
